﻿using System;
using EscuelaNet.Dominio.Conocimientos;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestConocimientos
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void PROBAR_INGRESAR_PRECIO_NEGATIVO()
        {
            Action action = () =>
            {
                var precio = new Precio(10, null);

            };
            Assert.ThrowsException<ArgumentException>(action);

            Action action1 = () =>
            {
                var precio1 = new Precio(-10, "Dolar");

            };
            Assert.ThrowsException<ExcepcionDeConocimiento>(action1);
        }
    }
}
