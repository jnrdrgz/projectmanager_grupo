﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Conocimientos
{
    public class ConocimientoPorPrecio
    {
        public Precio Precio { set; get;}
        public DateTime Fecha { set; get;}
        public Nivel Nivel { set; get;}

        
    }
}
