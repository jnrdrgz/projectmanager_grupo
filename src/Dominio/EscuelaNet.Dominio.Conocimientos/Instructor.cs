﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Conocimientos
{
  public  class Instructor
    {
        public string Nombre { get; set; }
        public Disponibilidad Disponibilidad { get; set; }
     
        public Instructor(string nombre)
        {
            this.Nombre = nombre ?? throw new System.ArgumentNullException(nameof(nombre));
        }
        private Instructor()
        {
        }
    }
}
