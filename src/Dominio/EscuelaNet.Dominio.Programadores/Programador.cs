﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Programadores
{
 
    public class Programador : Entity, IAggregateRoot
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Legajo { get; set; }
        public string Dni { get; set; }
        public string Rol { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public EstadoDeDisponibilidad Disponibilidad { get; set; }
        public IList<Conocimiento> Conocimientos { get; set; }

        public Programador(string nombre, string apellido, int legajo, string dni, string rol, DateTime fecnac)
        {
            this.Nombre = nombre;
            this.Apellido = apellido;
            this.Legajo = legajo;
            this.Dni = dni;
            this.Rol = rol;
            this.FechaNacimiento = fecnac;
            this.Disponibilidad = EstadoDeDisponibilidad.FullTime;
        }

        public Programador()
        {
            this.Disponibilidad = EstadoDeDisponibilidad.FullTime;
        }

        public void PushConocimiento(string nombre, GradoDeConocimiento grado)
        {
            if (this.Conocimientos == null)
            {
                this.Conocimientos = new List<Conocimiento>();
            }
            this.Conocimientos.Add(new Conocimiento(nombre,grado));
        }

        public void CambiarDisponibilidad(EstadoDeDisponibilidad disponibilidad)
        {
            
            if (this.Disponibilidad == EstadoDeDisponibilidad.FullTime)
            {
                this.Disponibilidad = disponibilidad;
            }
            else if(this.Disponibilidad == EstadoDeDisponibilidad.PartTime && disponibilidad != EstadoDeDisponibilidad.FullTime)
            {
                this.Disponibilidad = disponibilidad;
            }
        }

        public string ConsultarDisponibilidad()
        {
            return this.Disponibilidad.ToString();
        }

    }
}
