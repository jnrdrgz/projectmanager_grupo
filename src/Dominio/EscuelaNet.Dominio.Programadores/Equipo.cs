﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Programadores
{
    public class Equipo : Entity
    {
        public string Nombre { get; set; }
        public string NombrePais { get; set; }
        public string Hora { get; set; }
        public string RolDeEquipo { get; set; }
        public IList<Programador> Programadores { get; set; }
        public IList<Conocimiento> ConocimientosDeEquipo { get; set; }
         
        public Equipo(string nombre, string nombrepais, string hora)
        {
            this.Nombre = nombre ?? throw new System.ArgumentNullException(nameof(nombre)); 
            this.NombrePais = nombrepais ?? throw new System.ArgumentNullException(nameof(nombrepais)); 
            this.Hora = hora ?? throw new System.ArgumentNullException(nameof(hora)); 
        }

        private Equipo() {}


        public void PushProgramador(Programador programador)
        {
            foreach (var conocimiento in this.ConocimientosDeEquipo)
            {
                if (programador.Conocimientos == null || programador.Conocimientos.Contains(conocimiento))
                {
                    throw new Exception("No contiene los conocimientos del equipo");
                }
            }
            this.Programadores.Add(programador);
        }

        public void ConocimientoEquipo(string nombre, GradoDeConocimiento grado)
        {
            if (this.ConocimientosDeEquipo == null)
            {
                this.ConocimientosDeEquipo = new List<Conocimiento>();
            }

            this.ConocimientosDeEquipo.Add(new Conocimiento(nombre, grado));
        }
        
    }
}