﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Programadores
{
    public class Conocimiento : Entity
    {
        public string Descripcion { get; set; }
        public GradoDeConocimiento Grados { get; set; }

        public Conocimiento(string nombre, GradoDeConocimiento grado)
        {
            this.Descripcion = nombre ?? throw new System.ArgumentNullException(nameof(nombre));
            this.Grados = grado;
        }
    }
}
