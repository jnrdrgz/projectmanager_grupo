﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EscuelaNet.Dominio.Programadores.Test
{
    [TestClass]
    public class UnitTestProgramadores
    {
        [TestMethod]
        public void PROBAR_CREAR_UN_PROGRAMADOR()
        {
            var programador = new Programador("Cristian", "Martinez", 42201, "43501357", "Programador", DateTime.Now);
            Assert.AreEqual(EstadoDeDisponibilidad.FullTime.ToString(), programador.ConsultarDisponibilidad());

            programador.PushConocimiento("Java",GradoDeConocimiento.Advanced);
            programador.PushConocimiento("Python", GradoDeConocimiento.SemiSenior);

            var equipo = new Equipo("Alfa", "Italia", "2");

            equipo.PushProgramador(programador);

            equipo.ConocimientoEquipo("Java", GradoDeConocimiento.SemiSenior);
 
        }

        [TestMethod]
        public void PROBAR_CONSULTA_DISPONIBILIDAD()
        {
            var programador = new Programador();
            programador.CambiarDisponibilidad(EstadoDeDisponibilidad.FullTime);
            Assert.AreEqual(EstadoDeDisponibilidad.NoDisponible.ToString(), programador.ConsultarDisponibilidad());
            programador.CambiarDisponibilidad(EstadoDeDisponibilidad.NoDisponible);
            Assert.AreEqual(EstadoDeDisponibilidad.NoDisponible.ToString(), programador.ConsultarDisponibilidad());
        }
    }
}
