﻿using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EscuelaNet.Dominio.Clientes.Test
{
    [TestClass]
    public class UnitTestClientes
    {
        [TestMethod]
        public void PROBAR_CREAR_UNA_DIRECCION()
        {
            var direccion = new Direccion("domicilio1", "localidad1", "provincia1", "pais1");
            string direccionPrueba = "domicilio1, localidad1, provincia1, pais1";
            Assert.AreEqual(direccionPrueba, direccion.toString());
        }

        [TestMethod]
        public void PROBAR_CREAR_UN_CLIENTE()
        {
            var direccion = new Direccion("domicilio1", "localidad1", "provincia1", "pais1");
            string direccionPrueba = "domicilio1, localidad1, provincia1, pais1";

            var cliente = new Cliente("nombre", direccion, "email@email.com", TipoCliente.PersonaJuridica);

            Assert.AreEqual("nombre", cliente.NombreCompleto);
            Assert.AreEqual(direccionPrueba, cliente.Direcciones[0].toString());
            Assert.AreEqual("email@email.com", cliente.Email);
            Assert.AreEqual(TipoCliente.PersonaJuridica, cliente.Tipo);

        }


        [TestMethod]
        public void PROBAR_AGREGAR_DIRECCION()
        {
            var direccion = new Direccion("domicilio1", "localidad1", "provincia1", "pais1");
            var cliente = new Cliente("nombre", direccion, "email@email.com", TipoCliente.PersonaJuridica);

            var direccion2 = new Direccion("domicilio2", "localidad2", "provincia2", "pais2");
            cliente.AgregarDireccion(direccion2);

            string direccionPrueba2 = "domicilio2, localidad2, provincia2, pais2";
            Assert.AreEqual(direccionPrueba2, cliente.Direcciones[1].toString());
        }

        [TestMethod]
        public void PROBAR_AGREGAR_MISMA_DIRECCION()
        {
            var direccion = new Direccion("domicilio1", "localidad1", "provincia1", "pais1");
            var direccion2 = new Direccion("domicilio1", "localidad1", "provincia1", "pais1");

            var cliente = new Cliente("nombre", direccion, "email@email.com", TipoCliente.PersonaJuridica);

            cliente.AgregarDireccion(direccion2);

            Assert.AreEqual(1, cliente.Direcciones.Count);
        }



        [TestMethod]
        public void PROBAR_CREAR_UNA_UNIDAD()
        {
            var unidad = new UnidadDeNegocio("unidad1", "responsable1", "20-1122334455-7", "responsable@email.com", "03814112233");
            Assert.AreEqual("unidad1", unidad.NombreUnidad);
            Assert.AreEqual("responsable1", unidad.ResponsableDeUnidad);
            Assert.AreEqual("20-1122334455-7", unidad.Cuit);
            Assert.AreEqual("responsable@email.com", unidad.EmailResponsable);
            Assert.AreEqual("03814112233", unidad.TelefonoResponsable);

        }

        [TestMethod]
        public void PROBAR_AGREGAR_UNIDAD()
        {
            var direccion = new Direccion("domicilio1", "localidad1", "provincia1", "pais1");
            var cliente = new Cliente("nombre", direccion, "email@email.com", TipoCliente.PersonaJuridica);

            var unidad = new UnidadDeNegocio("unidad1", "responsable1", "20-1122334455-7", "responsable@email.com", "03814112233");
            cliente.AgregarUnidad(unidad);

            Assert.IsTrue(cliente.Unidades.Contains(unidad));

        }

        [TestMethod]
        public void PROBAR_CREAR_SOLICITUD()
        {
            var solicitud = new Solicitud("nombreSolicitud", "Descripcion");

            Assert.AreEqual("nombreSolicitud", solicitud.Nombre);
            Assert.AreEqual("Descripcion", solicitud.Descripcion);
            Assert.AreEqual(EstadoSolicitud.Borrador, solicitud.estado);

        }

        [TestMethod]
        public void PROBAR_AGREGAR_SOLICITUD()
        {
            var unidad = new UnidadDeNegocio("unidad1", "responsable1", "20-1122334455-7", "responsable@email.com", "03814112233");
            var solicitud = new Solicitud("nombreSolicitud", "Descripcion");
            unidad.AgregarSolicitud(solicitud);

            Assert.IsTrue(unidad.Solicitudes.Contains(solicitud));
        }

        [TestMethod]
        public void PROBAR_CAMBIAR_ESTADO_SOLICITUD()
        {
            var unidad = new UnidadDeNegocio("unidad1", "responsable1", "20-1122334455-7", "responsable@email.com", "03814112233");
            var solicitud = new Solicitud("NombreSolicitud", "Descripcion");
            unidad.AgregarSolicitud(solicitud);

            //de Borrador a Desarrollo
            unidad.CambiarEstadoSolicitud(solicitud, EstadoSolicitud.Desarrollo);
            Assert.AreEqual(EstadoSolicitud.Desarrollo, solicitud.estado);

            //de Desarrollo a Produccion
            unidad.CambiarEstadoSolicitud(solicitud, EstadoSolicitud.Produccion);
            Assert.AreEqual(EstadoSolicitud.Produccion, solicitud.estado);

            //de Produccion a Deprecado
            unidad.CambiarEstadoSolicitud(solicitud, EstadoSolicitud.Deprecado);
            Assert.AreEqual(EstadoSolicitud.Deprecado, solicitud.estado);

            //de Deprecado a Desarrollo NO DEBE CAMBIAR
            unidad.CambiarEstadoSolicitud(solicitud, EstadoSolicitud.Desarrollo);
            Assert.AreEqual(EstadoSolicitud.Deprecado, solicitud.estado);

            //de Deprecado a Produccion NO DEBE CAMBIAR
            unidad.CambiarEstadoSolicitud(solicitud, EstadoSolicitud.Produccion);
            Assert.AreEqual(EstadoSolicitud.Deprecado, solicitud.estado);

            var solicitud2 = new Solicitud("NombreSolicitud2", "Descripcion2");
            unidad.AgregarSolicitud(solicitud2);
            unidad.CambiarEstadoSolicitud(solicitud2, EstadoSolicitud.Desarrollo);

            //de Desarrollo a Abandonado
            unidad.CambiarEstadoSolicitud(solicitud2, EstadoSolicitud.Abandonado);
            Assert.AreEqual(EstadoSolicitud.Abandonado, solicitud2.estado);

            //de Abandonado a Borrador
            unidad.CambiarEstadoSolicitud(solicitud2, EstadoSolicitud.Borrador);
            Assert.AreEqual(EstadoSolicitud.Borrador, solicitud2.estado);

        }

    }
}
