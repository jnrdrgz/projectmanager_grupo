﻿using System.Collections.Generic;
using EscuelaNet.Dominio.SeedWoork;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public class Alumno : Entity
    {
        public string Nombre { get; set; }
        public IList<Conocimiento> Conocimientos { get; set; }

        public Alumno()
        {

        }
        public Alumno(string nombre) : this()
        {
            this.Nombre = nombre ?? throw new System.ArgumentNullException(nameof(nombre));

            this.Conocimientos = new List<Conocimiento>();
        }

    }
}