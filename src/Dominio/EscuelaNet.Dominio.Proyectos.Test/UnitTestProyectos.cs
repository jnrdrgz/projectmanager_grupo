﻿using System;
using EscuelaNet.Dominio.Programadores;
using EscuelaNet.Dominio.Capacitaciones;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Conocimiento = EscuelaNet.Dominio.Capacitaciones.Conocimiento;

namespace EscuelaNet.Dominio.Proyectos.Test
{
    [TestClass]
    public class UnitTestProyectos
    {
        [TestMethod]
        public void PROBAR_CREAR_UN_PROYECTO()
        {
            var proyecto = new Proyecto();
            Assert.AreEqual(EstadoDeProyecto.Diseno.ToString(), proyecto.ObtenerEstado());
        }
        [TestMethod]
        public void PROBAR_AGREGAR_ETAPAS()
        {
            var proyecto = new Proyecto();
            proyecto.PushEtapa("Prueba");
            proyecto.Etapas[0].CambiarDuracion(10);
            Assert.AreEqual("Prueba", proyecto.Etapas[0].Nombre);
            Assert.AreEqual(10, proyecto.Etapas[0].Duracion);
            Assert.AreEqual(10, proyecto.Duracion);
            

        }

        [TestMethod]
        public void PROBAR_INICIAR_UN_PROYECTO()
        {
            var proyecto = new Proyecto();
            proyecto.CambiarEstado(EstadoDeProyecto.Iniciado);
            Assert.AreEqual(EstadoDeProyecto.Iniciado.ToString(), proyecto.ObtenerEstado());
            proyecto.CambiarEstado(EstadoDeProyecto.Diseno);
            Assert.AreEqual(EstadoDeProyecto.Iniciado.ToString(), proyecto.ObtenerEstado());
        }
               

    }

}

