﻿using EscuelaNet.Dominio.SeedWoork;
using System.Collections.Generic;
using System.Linq;

namespace EscuelaNet.Dominio.Clientes
{
    public class Cliente : Entity, IAggregateRoot
   {
        public string NombreCompleto { get; set; }

        public IList<Direccion> Direcciones { get; private set; }

        public string Email { get; set; }

        public TipoCliente Tipo { get; set; }

        public IList<UnidadDeNegocio> Unidades { get; private set; } 

        private Cliente() { }

        public Cliente(string nombre, Direccion direccion, string email, TipoCliente tipo)
        {
            this.NombreCompleto = nombre ?? throw new System.ArgumentNullException(nameof(nombre));
            AgregarDireccion(direccion);
            this.Email = email ?? throw new System.ArgumentNullException(nameof(email));
            this.Tipo = tipo;
        }

        public void AgregarDireccion(Direccion direccion)
        {
            if (this.Direcciones == null)
            {
                this.Direcciones = new List<Direccion>();
            }
            if (!this.Direcciones.Any(x => x.toString() == direccion.toString()))
            {
                this.Direcciones.Add(direccion);
            }
            
        }

        public void AgregarUnidad(UnidadDeNegocio unidad)
        {
            if (this.Unidades == null)
            {
                this.Unidades = new List<UnidadDeNegocio>();
            }

            this.Unidades.Add(unidad);
        }

    }

}