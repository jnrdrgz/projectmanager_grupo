﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Clientes
{
    public class Solicitud : Entity
    {
        public string  Nombre { get; set; }

        public string Descripcion { get; set; }

        public EstadoSolicitud estado { get; set; }

        private Solicitud() { }

        public Solicitud(string nombre, string descripcion)
        {
            this.Nombre = nombre;
            this.Descripcion = descripcion;
            this.estado = EstadoSolicitud.Borrador;
        }
    }
}
