﻿using EscuelaNet.Dominio.SeedWoork;
using System.Collections.Generic;

namespace EscuelaNet.Dominio.Clientes
{
    public class UnidadDeNegocio : Entity
    {
        public string NombreUnidad { get; set; }

        public string ResponsableDeUnidad { get; set; }

        public string Cuit { get; set; }

        public string EmailResponsable { get; set; }

        public string TelefonoResponsable { get; set; }

        public IList<Solicitud> Solicitudes { get; set; }

        private UnidadDeNegocio() { }

        public UnidadDeNegocio(string nombre, string responsable, string cuit, string email, string telefono)
        {
            this.NombreUnidad = nombre ?? throw new System.ArgumentNullException(nameof(nombre));
            this.ResponsableDeUnidad = responsable ?? throw new System.ArgumentNullException(nameof(responsable));
            this.Cuit = cuit ?? throw new System.ArgumentNullException(nameof(cuit));
            this.EmailResponsable = email ?? throw new System.ArgumentNullException(nameof(email));
            this.TelefonoResponsable = telefono ?? throw new System.ArgumentNullException(nameof(telefono));
        }

        public void AgregarSolicitud(Solicitud solicitud)
        {
            if (this.Solicitudes==null)
            {
                this.Solicitudes = new List<Solicitud>();
            }
            this.Solicitudes.Add(solicitud);
        }

        public void CambiarEstadoSolicitud(Solicitud solicitud, EstadoSolicitud estadoNuevo)
        {
            if (this.Solicitudes.Contains(solicitud))
            {
                if (solicitud.estado==EstadoSolicitud.Borrador && estadoNuevo==EstadoSolicitud.Desarrollo)
                {
                    solicitud.estado = EstadoSolicitud.Desarrollo;
                }
                else if (solicitud.estado == EstadoSolicitud.Desarrollo && estadoNuevo == EstadoSolicitud.Produccion)
                {
                    solicitud.estado = EstadoSolicitud.Produccion;
                }
                else if (solicitud.estado == EstadoSolicitud.Produccion && estadoNuevo == EstadoSolicitud.Deprecado)
                {
                    solicitud.estado = EstadoSolicitud.Deprecado;
                }
                else if (solicitud.estado == EstadoSolicitud.Desarrollo && estadoNuevo == EstadoSolicitud.Abandonado)
                {
                    solicitud.estado = EstadoSolicitud.Abandonado;
                }
                else if (solicitud.estado == EstadoSolicitud.Abandonado && estadoNuevo == EstadoSolicitud.Borrador)
                {
                    solicitud.estado = EstadoSolicitud.Borrador;
                }
            }

        }


    }
}
