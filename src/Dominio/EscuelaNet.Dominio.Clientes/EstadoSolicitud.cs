﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Clientes
{
    public enum EstadoSolicitud
    {
        Borrador,
        Desarrollo,
        Produccion,
        Deprecado,
        Abandonado
    }
}
